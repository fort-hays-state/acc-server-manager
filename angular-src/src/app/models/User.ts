import { ObjectId } from 'mongodb';

export interface User {
  name?: string;
  username?: string;
  email?: string;
  password?: string;
  authorized?: boolean;
  owner?: boolean;
  _id?: ObjectId;
}
