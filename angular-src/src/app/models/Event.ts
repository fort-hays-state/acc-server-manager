export interface Session {
  sessionType?: any;
  dayOfWeekend?: Number;
  hourOfDay?: Number;
  sessionDurationMinutes?: Number;
  timeMultiplier?: Number;
}

export interface EventRules {
  qualifyStandingType?: Number;
  pitWindowLengthSec?: Number;
  driverStintTimeSec?: Number;
  mandatoryPitstopCount?: Number;
  maxTotalDrivingTime?: Number;
  maxDriversCount?: Number;
  isRefuellingAllowedInRace?: Number;
  isRefuellingTimeFixed?: Boolean;
  isMandatoryPitstopRefuellingRequired?: Boolean;
  isMandatoryPitstopTyreChangeRequired?: Boolean;
  isMandatoryPitstopSwapDriverRequired?: Boolean;
  tyreSetCount?: Number;
}

export interface EventDetail {
  track?: String;
  preRaceWaitingTimeSeconds?: Number;
  sessionOverTimeSeconds?: Number;
  ambientTemp?: Number;
  cloudLevel?: Number;
  rain?: Number;
  weatherRandomness?: Number;
  postQualySeconds?: Number;
  postRaceSeconds?: Number;
  sessions?: Session[];
  configVersion?: Number;
}

export interface Event {
  _id?: Object;
  name?: string;
  rules?: EventRules;
  detail?: EventDetail;
  hide?: Boolean;
}
