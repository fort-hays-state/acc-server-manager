export interface Track {
  _id?: Object;
  name?: string;
  pitBoxes?: number;
  privateServerSlots?: number;
  season?: number;
  identifier?: string;
  hide?: boolean;
}
