export interface AssistRules {
  stabilityControlLevelMax?: number;
  disableAutoSteer?: number;
  disableIdealLine?: number;
  disableAutoPitLimiter?: number;
  disableAutoGear?: number;
  disableAutoClutch?: number;
  disableAutoEngineStart?: number;
  disableAutoWiper?: number;
  disableAutoLights?: number;
}

export interface Config {
  tcpPort?: number;
  udpPort?: number;
  registerToLobby?: number;
  maxConnections?: number;
  lanDiscovery?: number;
  configVersion?: number;
}

export interface Settings {
  serverName?: string;
  adminPassword?: string;
  carGroup?: string;
  trackMedalsRequirement?: number;
  safetyRatingRequirement?: number;
  racecraftRatingRequirement?: number;
  password?: string;
  spectatorPassword?: string;
  maxCarSlots?: number;
  dumpLeaderboards?: number;
  dumpEntryList?: number;
  isRaceLocked?: number;
  randomizeTrackWhenEmpty?: number;
  centralEntryListPath?: string;
  allowAutoDQ?: number;
  shortFormationLap?: number;
  formationLapType?: number;
}

export interface Server {
  _id?: any;
  online?: boolean;
  name?: string;
  eventId?: any;
  hide?: boolean;
  configuration?: Config;
  settings?: Settings;
  assistRules?: AssistRules;
}
