import { Component, OnInit } from '@angular/core';
import { User } from '../../models/User';
import { AuthenticationService } from '../../services/authentication.service';
import { FlashMessagesService } from 'flash-messages-angular';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {
  users: User[];
  loaded: boolean = false;

  constructor(
    private _authenticationService: AuthenticationService,
    private _flashMessagesService: FlashMessagesService
  ) {}

  ngOnInit(): void {
    this._authenticationService.getUsers().subscribe((users) => {
      this.users = users;
      this.sortUsers();
      this.loaded = true;
    });
  }

  sortUsers() {
    this.users.sort((a, b) => {
      return a.name.localeCompare(b.name);
    });
  }

  onDeleteUser(user: User) {
    this.users.forEach((cur, index) => {
      if (user._id === cur._id) {
        this.users.splice(index, 1);
      }
    });
  }

  onUpdatedUser(user: User) {
    this.users.forEach((cur, index) => {
      if (user._id === cur._id) {
        this.users.splice(index, 1);
        this.users.unshift(user);
      }
    });
    this.sortUsers();
  }
}
