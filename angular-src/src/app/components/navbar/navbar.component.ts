import { Component, OnInit } from '@angular/core';
import { faCog, faUser } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

import { FlashMessagesService } from 'flash-messages-angular';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  faCog = faCog;
  faUser = faUser;

  constructor(
    public authenticationService: AuthenticationService,
    private _flashMessagesService: FlashMessagesService,
    private _router: Router
  ) {}

  ngOnInit(): void {}

  onLogoutClick() {
    this.authenticationService.logout();
    this._flashMessagesService.show('You have been logged out', {
      cssClass: 'alert-success',
      timeout: 3000,
    });
    this._router.navigate(['login']);
    return false;
  }
}
