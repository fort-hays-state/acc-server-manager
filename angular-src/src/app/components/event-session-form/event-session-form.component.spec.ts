import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventSessionFormComponent } from './event-session-form.component';

describe('EventSessionFormComponent', () => {
  let component: EventSessionFormComponent;
  let fixture: ComponentFixture<EventSessionFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventSessionFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventSessionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
