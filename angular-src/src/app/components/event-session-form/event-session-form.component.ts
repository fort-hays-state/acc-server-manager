import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';
import { Session, Event } from '../../models/Event';

@Component({
  selector: 'app-event-session-form',
  templateUrl: './event-session-form.component.html',
  styleUrls: ['./event-session-form.component.css'],
})
export class EventSessionFormComponent implements OnInit {
  sessions: Session[];

  constructor() {}

  ngOnInit(): void {
    this.sessions = [
      {
        sessionType: null,
        dayOfWeekend: null,
        hourOfDay: null,
        sessionDurationMinutes: null,
        timeMultiplier: null,
      },
      {
        sessionType: null,
        dayOfWeekend: null,
        hourOfDay: null,
        sessionDurationMinutes: null,
        timeMultiplier: null,
      },
      {
        sessionType: null,
        dayOfWeekend: null,
        hourOfDay: null,
        sessionDurationMinutes: null,
        timeMultiplier: null,
      },
    ];
  }
}
