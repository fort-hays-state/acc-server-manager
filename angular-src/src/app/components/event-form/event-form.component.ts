import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  ViewChild,
} from '@angular/core';
import { FlashMessagesService } from 'flash-messages-angular';
import { EventService } from '../../services/event.service';
import { TracksService } from '../../services/tracks.service';
import { faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons';
import { Session, Event, EventRules, EventDetail } from '../../models/Event';
import { Track } from '../../models/Track';

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.css'],
})
export class EventFormComponent implements OnInit {
  @ViewChild('eventForm') form: any;
  @Output() notifyChild: EventEmitter<any> = new EventEmitter();
  @Output() newEvent: EventEmitter<Event> = new EventEmitter();
  @Output() updatedEvent: EventEmitter<Event> = new EventEmitter();
  @Input() currentEvent: Event;
  @Input() isEditing: boolean;
  iExpand = faAngleDown;
  iCollapse = faAngleUp;
  showEventForm: boolean = false;
  tracks: Track[];
  loaded: boolean = false;

  constructor(
    private _flashMessagesService: FlashMessagesService,
    private _eventService: EventService,
    private _tracksService: TracksService
  ) {}

  ngOnInit(): void {
    this._tracksService.getTracks().subscribe((tracks) => {
      this.tracks = tracks;
      this.sortTracks();
      this.loaded = true;
    });
  }

  sortTracks() {
    this.tracks.sort((a, b) => {
      let res = a.name.localeCompare(b.name);
      if (res === 0) res = a.season - b.season;
      return res;
    });
  }

  addEvent(event: Event, sessions: Session[]) {
    event.detail.sessions = sessions;
    event.hide = true;
    this._eventService.addEvent(event).subscribe((data) => {
      if (data.success) {
        this._flashMessagesService.show(data.msg, {
          cssClass: 'alert-success',
          timeout: 3000,
        });
        this.form.reset();
        this.showEventForm = false;
        this.newEvent.emit(data.event);
      } else {
        this._flashMessagesService.show(data.msg, {
          cssClass: 'alert-danger',
          timeout: 5000,
        });
      }
    });
  }

  updateEvent() {}
}
