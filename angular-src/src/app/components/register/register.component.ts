import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FlashMessagesService } from 'flash-messages-angular';
import { Router } from '@angular/router';

import { AuthenticationService } from '../../services/authentication.service';

import { User } from '../../models/User';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  user: User = {
    name: '',
    username: '',
    email: '',
    password: '',
  };
  passwordConfirm: string = '';

  constructor(
    private _flashMessageService: FlashMessagesService,
    private _authenticationService: AuthenticationService,
    private _router: Router
  ) {}

  ngOnInit(): void {}

  onRegisterSubmit() {
    // Verify User enters two identical passwords
    if (this.user.password !== this.passwordConfirm) {
      return this._flashMessageService.show('Passwords do not match!', {
        cssClass: 'alert-danger',
        timeout: 3000,
      });
    }

    // Register user with backend
    this._authenticationService.registerUser(this.user).subscribe((data) => {
      if (data.success) {
        this._flashMessageService.show(
          'Registration request accepted, please wait for admin approval.',
          {
            cssClass: 'alert-success',
            timeout: 5000,
          }
        );
        this._router.navigate(['/login']);
      } else if (data.err.keyPattern.hasOwnProperty('email')) {
        this._flashMessageService.show(
          'An account with this Email address already exists.',
          {
            cssClass: 'alert-danger',
            timeout: 5000,
          }
        );
        this._router.navigate(['/register']);
      } else if (data.err.keyPattern.hasOwnProperty('username')) {
        this._flashMessageService.show(
          'An account with this Username address already exists.',
          {
            cssClass: 'alert-danger',
            timeout: 5000,
          }
        );
        this._router.navigate(['/register']);
      } else {
        this._flashMessageService.show(
          'Registration unsuccessful! Contact an admin.',
          {
            cssClass: 'alert-danger',
            timeout: 5000,
          }
        );
        this._router.navigate(['/register']);
      }
    });
  }
}
