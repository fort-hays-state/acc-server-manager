import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FlashMessagesService } from 'flash-messages-angular';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;

  constructor(
    private _authenticationService: AuthenticationService,
    private _flashMessagesService: FlashMessagesService,
    private _router: Router
  ) {}

  ngOnInit(): void {}

  onLoginSubmit() {
    const user = {
      username: this.username,
      password: this.password,
    };
    this._authenticationService.authenticateUser(user).subscribe((data) => {
      if (data.success) {
        if (data.user.authorized) {
          this._authenticationService.storeUserData(data.token, data.user);
          this._flashMessagesService.show('Login successful', {
            cssClass: 'alert-success',
            timeout: 3000,
          });
          this._router.navigate(['servers']);
        } else {
          this._flashMessagesService.show(
            'Account has not yet been authorized',
            {
              cssClass: 'alert-danger',
              timeout: 3000,
            }
          );
          this._router.navigate(['login']);
        }
      } else {
        this._flashMessagesService.show('Incorrect login credentials', {
          cssClass: 'alert-danger',
          timeout: 3000,
        });
        this._router.navigate(['login']);
      }
    });
  }
}
