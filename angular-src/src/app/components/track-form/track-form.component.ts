import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  ViewChild,
} from '@angular/core';
import { FlashMessagesService } from 'flash-messages-angular';
import { faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons';
import { Track } from '../../models/Track';
import { TracksService } from '../../services/tracks.service';

@Component({
  selector: 'app-track-form',
  templateUrl: './track-form.component.html',
  styleUrls: ['./track-form.component.css'],
})
export class TrackFormComponent implements OnInit {
  @ViewChild('trackForm') form: any;
  @Output() newTrack: EventEmitter<Track> = new EventEmitter();
  @Output() updatedTrack: EventEmitter<Track> = new EventEmitter();
  @Input() currentTrack: Track;
  @Input() isEditing: boolean;
  iExpand = faAngleDown;
  iCollapse = faAngleUp;
  showTrackForm: boolean = false;

  constructor(
    private _tracksService: TracksService,
    private _flashMessagesService: FlashMessagesService
  ) {}

  ngOnInit(): void {}

  addTrack({ value, valid }: { value: Track; valid: boolean }) {
    if (!valid) {
      this._flashMessagesService.show('Form invalid', {
        cssClass: 'alert-danger',
        timeout: 5000,
      });
    } else {
      this._tracksService.addTrack(value).subscribe((data) => {
        if (data.success) {
          this._flashMessagesService.show(data.msg, {
            cssClass: 'alert-success',
            timeout: 3000,
          });
          this.form.reset();
          this.newTrack.emit(data.track);
        } else {
          this._flashMessagesService.show(data.msg, {
            cssClass: 'alert-danger',
            timeout: 5000,
          });
        }
      });
    }
  }

  updateTrack() {
    this._tracksService.updateTrack(this.currentTrack).subscribe((data) => {
      if (data.success) {
        this._flashMessagesService.show(data.msg, {
          cssClass: 'alert-success',
          timeout: 3000,
        });
        this.isEditing = false;
        this.form.reset();
        data.track.hide = false;
        this.updatedTrack.emit(data.track);
      } else {
        this._flashMessagesService.show(data.msg, {
          cssClass: 'alert-danger',
          timeout: 5000,
        });
      }
    });
  }
}
