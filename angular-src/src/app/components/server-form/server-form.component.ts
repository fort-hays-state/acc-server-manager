import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
} from '@angular/core';
import { Server } from '../../models/Server';
import { Event } from '../../models/Event';
import { faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons';
import { EventService } from '../../services/event.service';
import { ServerService } from '../../services/server.service';
import { FlashMessagesService } from 'flash-messages-angular';

@Component({
  selector: 'app-server-form',
  templateUrl: './server-form.component.html',
  styleUrls: ['./server-form.component.css'],
})
export class ServerFormComponent implements OnInit {
  @ViewChild('serverForm') form: any;
  @Output() newServer: EventEmitter<Server> = new EventEmitter();
  @Output() updatedServer: EventEmitter<Server> = new EventEmitter();
  @Input() currentServer: Server;
  @Input() isEditing: boolean;
  iExpand = faAngleDown;
  iCollapse = faAngleUp;
  showServerForm: boolean = false;
  loaded: boolean = false;
  events: Event[];

  constructor(
    private _flashMessagesService: FlashMessagesService,
    private _serverService: ServerService,
    private _eventService: EventService
  ) {}

  ngOnInit(): void {
    this._eventService.getEvents().subscribe((events) => {
      this.events = events;
      this.sortEvents();
      this.loaded = true;
    });
  }

  sortEvents() {
    this.events.sort((a, b) => {
      return a.name.localeCompare(b.name);
    });
  }

  addServer(server: Server) {
    server.hide = true;
    this._serverService.addServer(server).subscribe((data) => {
      if (data.success) {
        this._flashMessagesService.show(data.msg, {
          cssClass: 'alert-success',
          timeout: 3000,
        });
        this.showServerForm = false;
        this.form.reset();
        this.newServer.emit(data.server);
      } else {
        this._flashMessagesService.show(data.msg, {
          cssClass: 'alert-danger',
          timeout: 5000,
        });
      }
    });
  }

  updateServer() {
    const setUpdate = {
      $set: { ...this.currentServer },
    };
    this._serverService
      .updateServer(this.currentServer, setUpdate)
      .subscribe((data) => {
        if (data.success) {
          this._flashMessagesService.show(data.msg, {
            cssClass: 'alert-success',
            timeout: 3000,
          });
          this.isEditing = false;
          this.form.reset();
          data.server.hide = false;
          this.updatedServer.emit(data.server);
        } else {
          this._flashMessagesService.show(data.msg, {
            cssClass: 'alert-danger',
            timeout: 5000,
          });
        }
      });
  }
}
