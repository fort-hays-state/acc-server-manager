import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../models/User';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { AuthenticationService } from '../../services/authentication.service';
import { FlashMessagesService } from 'flash-messages-angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  @Output() deletedUser: EventEmitter<User> = new EventEmitter();
  @Output() updatedUser: EventEmitter<User> = new EventEmitter();
  @Input() user: User;

  iDelete = faTrashAlt;
  currentRoute: string;

  constructor(
    public authenticationService: AuthenticationService,
    private _flashMessagesService: FlashMessagesService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.currentRoute = this._router.url;

    if (!this.user) {
      this.authenticationService.getProfile().subscribe(
        (profile) => {
          this.user = profile.user;
        },
        (err) => {
          console.log(err);
          return false;
        }
      );
    }
  }

  deleteUser(user: User) {
    const username = user.username;
    this.authenticationService.deleteUser(user).subscribe((data) => {
      if (!data.success) {
        this._flashMessagesService.show(data.msg, {
          cssClass: 'alert-danger',
          timeout: 5000,
        });
      } else {
        this._flashMessagesService.show(`Successfully deleted ${username}`, {
          cssClass: 'alert-success',
          timeout: 3000,
        });
        this.deletedUser.emit(user);
      }
    });
  }

  onAuthorizedCheck(user: User) {
    const setUpdate = { $set: { authorized: !user.authorized } };
    this.authenticationService
      .updateUser(user._id, setUpdate)
      .subscribe((data) => {
        if (data.success) {
          this._flashMessagesService.show(data.msg, {
            cssClass: 'alert-success',
            timeout: 3000,
          });
          this.updatedUser.emit(data.user);
        } else {
          this._flashMessagesService.show(data.msg, {
            cssClass: 'alert-danger',
            timeout: 5000,
          });
        }
      });
  }

  onOwnerCheck(user: User) {
    const setUpdate = {
      $set: { owner: !user.owner },
    };
    this.authenticationService
      .updateUser(user._id, setUpdate)
      .subscribe((data) => {
        if (data.success) {
          this._flashMessagesService.show(data.msg, {
            cssClass: 'alert-success',
            timeout: 3000,
          });
          this.updatedUser.emit(data.user);
        } else {
          this._flashMessagesService.show(data.msg, {
            cssClass: 'alert-danger',
            timeout: 5000,
          });
        }
      });
  }
}
