import { Component, OnInit } from '@angular/core';
import { Server } from '../../models/Server';
import { Event } from '../../models/Event';
import {
  faAngleDown,
  faAngleUp,
  faTrashAlt,
  faPencilAlt,
  faStop,
  faPlay,
  faCircle,
} from '@fortawesome/free-solid-svg-icons';
import { FlashMessagesService } from 'flash-messages-angular';
import { EventService } from '../../services/event.service';
import { ServerService } from '../../services/server.service';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css'],
})
export class ServersComponent implements OnInit {
  iExpand = faAngleDown;
  iCollapse = faAngleUp;
  iDelete = faTrashAlt;
  iEdit = faPencilAlt;
  iStop = faStop;
  iPlay = faPlay;
  iStatus = faCircle;

  defaultServer: Server = {
    online: false,
    name: '',
    eventId: null,
    hide: true,
    configuration: {
      tcpPort: null,
      udpPort: null,
      registerToLobby: null,
      maxConnections: null,
      lanDiscovery: null,
      configVersion: null,
    },
    assistRules: {
      stabilityControlLevelMax: 100,
      disableAutoSteer: 0,
      disableIdealLine: 0,
      disableAutoPitLimiter: 0,
      disableAutoGear: 0,
      disableAutoClutch: 0,
      disableAutoEngineStart: 0,
      disableAutoWiper: 0,
      disableAutoLights: 0,
    },
    settings: {
      serverName: '',
      adminPassword: '',
      carGroup: '',
      trackMedalsRequirement: null,
      safetyRatingRequirement: null,
      racecraftRatingRequirement: null,
      password: '',
      spectatorPassword: '',
      maxCarSlots: null,
      dumpLeaderboards: null,
      dumpEntryList: null,
      isRaceLocked: null,
      randomizeTrackWhenEmpty: null,
      centralEntryListPath: '',
      allowAutoDQ: null,
      shortFormationLap: null,
      formationLapType: null,
    },
  };

  currentServer: Server = this.defaultServer;
  events: Event[];
  servers: Server[];
  isEditing: boolean = false;
  loaded: boolean = false;

  constructor(
    private _flashMessagesService: FlashMessagesService,
    private _serverService: ServerService,
    private _eventService: EventService
  ) {}

  ngOnInit(): void {
    this._serverService.getServers().subscribe((servers) => {
      this.servers = servers;
      this._eventService.getEvents().subscribe((events) => {
        this.events = events;
        this.loaded = true;
      });
    });
  }

  onNewServer(server: Server) {
    this.servers.unshift(server);
    this.isEditing = false;
    this.currentServer = this.defaultServer;
  }

  onUpdatedServer(server: Server) {
    this.servers.forEach((cur, index) => {
      if (server._id === cur._id) {
        this.servers.splice(index, 1, server);
      }
    });
  }

  editServer(server: Server) {
    this.isEditing = true;
    this.currentServer = server;
  }

  deleteServer(server: Server) {
    const serverName = server.name;
    this._serverService.deleteServer(server).subscribe((data) => {
      if (!data.success) {
        this._flashMessagesService.show(data.msg, {
          cssClass: 'alert-danger',
          timeout: 5000,
        });
      } else {
        this._flashMessagesService.show(`Successfully deleted ${serverName}`, {
          cssClass: 'alert-success',
          timeout: 3000,
        });
        this.servers.forEach((cur, index) => {
          if (server._id === cur._id) {
            this.servers.splice(index, 1);
          }
        });
      }
    });
  }

  getEventDisplayName(id: any) {
    this.events.forEach((value, index) => {
      if (value._id === id) return value.name;
    });
  }

  valueSwitcher(value: number | string): number | string {
    if (typeof value === typeof 0) {
      switch (value) {
        case 0:
          return 'No';
        case 1:
          return 'Yes';
      }
    } else if (typeof value === typeof 'hi') {
      switch (value) {
        case 'No':
          return 0;
        case 'Yes':
          return 1;
      }
    }
  }
}
