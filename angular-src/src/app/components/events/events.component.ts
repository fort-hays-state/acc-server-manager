import { Component, OnInit, Input } from '@angular/core';
import { Session, Event, EventRules, EventDetail } from '../../models/Event';
import {
  faAngleDown,
  faAngleUp,
  faTrashAlt,
  faPencilAlt,
} from '@fortawesome/free-solid-svg-icons';
import { FlashMessagesService } from 'flash-messages-angular';
import { EventService } from '../../services/event.service';
import { Track } from '../../models/Track';
import { TracksService } from '../../services/tracks.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css'],
})
export class EventsComponent implements OnInit {
  iExpand = faAngleDown;
  iCollapse = faAngleUp;
  iDelete = faTrashAlt;
  iEdit = faPencilAlt;
  defaultEvent: Event = {
    name: '',
    rules: {
      qualifyStandingType: null,
      pitWindowLengthSec: null,
      driverStintTimeSec: null,
      mandatoryPitstopCount: null,
      maxTotalDrivingTime: null,
      maxDriversCount: null,
      isRefuellingAllowedInRace: null,
      isRefuellingTimeFixed: null,
      isMandatoryPitstopRefuellingRequired: null,
      isMandatoryPitstopTyreChangeRequired: null,
      isMandatoryPitstopSwapDriverRequired: null,
      tyreSetCount: null,
    },
    detail: {
      track: '',
      preRaceWaitingTimeSeconds: null,
      sessionOverTimeSeconds: null,
      ambientTemp: null,
      cloudLevel: null,
      rain: null,
      weatherRandomness: null,
      postQualySeconds: null,
      postRaceSeconds: null,
      sessions: [
        {
          sessionType: '',
          dayOfWeekend: null,
          hourOfDay: null,
          sessionDurationMinutes: null,
          timeMultiplier: null,
        },
        {
          sessionType: '',
          dayOfWeekend: null,
          hourOfDay: null,
          sessionDurationMinutes: null,
          timeMultiplier: null,
        },
        {
          sessionType: '',
          dayOfWeekend: null,
          hourOfDay: null,
          sessionDurationMinutes: null,
          timeMultiplier: null,
        },
      ],
    },
  };
  currentEvent: Event = this.defaultEvent;
  tracks: Track[];
  events: Event[];
  isEditing: boolean = false;
  loaded: boolean = false;
  constructor(
    private _eventService: EventService,
    private _flashMessagesService: FlashMessagesService,
    private _tracksService: TracksService
  ) {}

  ngOnInit(): void {
    this._eventService.getEvents().subscribe((events) => {
      this.events = events;
      this.sortEvents();
      this._tracksService.getTracks().subscribe((tracks) => {
        this.tracks = tracks;
        this.loaded = true;
      });
    });
  }

  onNewEvent(event: Event) {
    this.events.unshift(event);
    this.sortEvents();
    this.isEditing = false;
    this.currentEvent = this.defaultEvent;
  }

  onUpdatedEvent(event: Event) {}

  editEvent(event: Event) {}

  deleteEvent(event: Event) {
    const eventName = event.name;
    this._eventService.deleteEvent(event).subscribe((data) => {
      if (!data.success) {
        this._flashMessagesService.show(data.msg, {
          cssClass: 'alert-danger',
          timeout: 5000,
        });
      } else {
        this._flashMessagesService.show(`Successfully deleted ${eventName}`, {
          cssClass: 'alert-success',
          timeout: 3000,
        });
      }
    });
    this.events.forEach((cur, index) => {
      if (event._id === cur._id) {
        this.events.splice(index, 1);
      }
    });
  }

  sortEvents() {
    this.events.sort((a, b) => {
      return a.name.localeCompare(b.name);
    });
  }

  getTrackDisplayName(identifier: String): String {
    let trackName;
    this.tracks.forEach((value, index) => {
      if (value.identifier === identifier) {
        trackName = value.name;
        trackName += ' ' + value.season;
      }
    });
    return trackName;
  }

  getSessionTypeDisplayName(identifier: String): String {
    switch (identifier) {
      case 'P':
        return 'Practice';
      case 'Q':
        return 'Qualifying';
      case 'R':
        return 'Race';
      default:
        return 'Error';
    }
  }

  getDayDisplayName(identifier: Number): String {
    switch (identifier) {
      case 1:
        return 'Friday';
      case 2:
        return 'Saturday';
      case 3:
        return 'Sunday';
      default:
        return 'Error';
    }
  }
}
