import { Component, OnInit } from '@angular/core';
import { Track } from '../../models/Track';
import {
  faAngleDown,
  faAngleUp,
  faTrashAlt,
  faPencilAlt,
} from '@fortawesome/free-solid-svg-icons';
import { TracksService } from '../../services/tracks.service';
import { FlashMessagesService } from 'flash-messages-angular';

@Component({
  selector: 'app-tracks',
  templateUrl: './tracks.component.html',
  styleUrls: ['./tracks.component.css'],
})
export class TracksComponent implements OnInit {
  iExpand = faAngleDown;
  iCollapse = faAngleUp;
  iDelete = faTrashAlt;
  iEdit = faPencilAlt;

  tracks: Track[];
  currentTrack: Track = {
    name: '',
    season: null,
    pitBoxes: null,
    privateServerSlots: null,
  };
  isEditing: boolean = false;
  loaded: boolean = false;

  constructor(
    private _tracksService: TracksService,
    private _flashMessagesService: FlashMessagesService
  ) {}

  ngOnInit(): void {
    this._tracksService.getTracks().subscribe((tracks) => {
      this.tracks = tracks;
      this.sortTracks();
      this.loaded = true;
    });
  }

  sortTracks() {
    this.tracks.sort((a, b) => {
      let res = a.name.localeCompare(b.name);
      if (res === 0) res = a.season - b.season;
      return res;
    });
  }

  onNewTrack(track: Track) {
    this.tracks.unshift(track);
    this.sortTracks();
    this.currentTrack = {
      name: '',
      season: null,
      pitBoxes: null,
      privateServerSlots: null,
    };
  }

  onUpdatedTrack(track: Track) {
    this.tracks.forEach((cur, index) => {
      if (track._id === cur._id) {
        this.tracks.splice(index, 1);
        this.tracks.unshift(track);
        this.isEditing = false;
        this.currentTrack = {
          name: '',
          season: null,
          pitBoxes: null,
          privateServerSlots: null,
        };
      }
    });
    this.sortTracks();
  }

  editTrack(track: Track) {
    this.isEditing = true;
    this.currentTrack = track;
  }

  deleteTrack(track: Track) {
    const id = track.identifier;
    this._tracksService.deleteTrack(track).subscribe((data) => {
      if (!data.success) {
        this._flashMessagesService.show(data.msg, {
          cssClass: 'alert-danger',
          timeout: 5000,
        });
      } else {
        this._flashMessagesService.show(`Successfully deleted ${id}`, {
          cssClass: 'alert-success',
          timeout: 3000,
        });
        this.tracks.forEach((cur, index) => {
          if (track.identifier === cur.identifier) {
            this.tracks.splice(index, 1);
          }
        });
      }
    });
  }
}
