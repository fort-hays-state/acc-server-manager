import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  FlashMessagesModule,
  FlashMessagesComponent,
} from 'flash-messages-angular';

import { AuthenticationService } from './services/authentication.service';
import { AuthenticationGuard } from './guards/authentication.guard';
import { TracksService } from './services/tracks.service';
import { EventService } from './services/event.service';
import { ServerService } from './services/server.service';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ServersComponent } from './components/servers/servers.component';
import { EventsComponent } from './components/events/events.component';
import { SettingsComponent } from './components/settings/settings.component';
import { ProfileComponent } from './components/profile/profile.component';
import { TrackFormComponent } from './components/track-form/track-form.component';
import { TracksComponent } from './components/tracks/tracks.component';
import { UsersComponent } from './components/users/users.component';
import { ProfileHeaderComponent } from './components/profile-header/profile-header.component';
import { EventFormComponent } from './components/event-form/event-form.component';
import { EventSessionFormComponent } from './components/event-session-form/event-session-form.component';
import { ServerFormComponent } from './components/server-form/server-form.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    ServersComponent,
    EventsComponent,
    SettingsComponent,
    ProfileComponent,
    TrackFormComponent,
    TracksComponent,
    UsersComponent,
    ProfileHeaderComponent,
    EventFormComponent,
    EventSessionFormComponent,
    ServerFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    FontAwesomeModule,
    FlashMessagesModule.forRoot(),
  ],
  providers: [
    AuthenticationService,
    AuthenticationGuard,
    TracksService,
    EventService,
    ServerService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
