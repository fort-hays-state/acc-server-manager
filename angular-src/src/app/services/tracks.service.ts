import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Track } from '../models/Track';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root',
})
export class TracksService {
  baseSettingsUrl: string = 'http://localhost:3000/tracks';

  constructor(
    private _http: HttpClient,
    private _authenticationService: AuthenticationService
  ) {}

  getTracks(): Observable<Track[]> {
    this._authenticationService.loadToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this._authenticationService.authToken,
      }),
    };
    const getTracksUrl = `${this.baseSettingsUrl}/track`;
    return this._http.get<Track[]>(getTracksUrl, httpOptions);
  }

  addTrack(track: Track): Observable<any> {
    this._authenticationService.loadToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this._authenticationService.authToken,
      }),
    };
    const addTrackUrl = `${this.baseSettingsUrl}/track`;
    return this._http.post<Track>(addTrackUrl, track, httpOptions);
  }

  updateTrack(track: Track): Observable<any> {
    this._authenticationService.loadToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this._authenticationService.authToken,
      }),
    };
    const updateTrackUrl = `${this.baseSettingsUrl}/track/${track.identifier}`;
    return this._http.put<Track>(updateTrackUrl, track, httpOptions);
  }

  deleteTrack(track: Track): Observable<any> {
    this._authenticationService.loadToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this._authenticationService.authToken,
      }),
    };
    const deleteTrackUrl = `${this.baseSettingsUrl}/track/${track.identifier}`;
    return this._http.delete<Track>(deleteTrackUrl, httpOptions);
  }
}
