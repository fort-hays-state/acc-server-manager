import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';
import { Server } from '../models/Server';

@Injectable({
  providedIn: 'root',
})
export class ServerService {
  baseServerUrl: string = 'http://localhost:3000/servers';

  constructor(
    private _http: HttpClient,
    private _authenticationService: AuthenticationService
  ) {}

  addServer(server: Server): Observable<any> {
    this._authenticationService.loadToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this._authenticationService.authToken,
      }),
    };
    return this._http.post<Server>(this.baseServerUrl, server, httpOptions);
  }

  getServers(): Observable<Server[]> {
    this._authenticationService.loadToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this._authenticationService.authToken,
      }),
    };
    return this._http.get<Server[]>(this.baseServerUrl, httpOptions);
  }

  updateServer(id, updateQuery): Observable<any> {
    this._authenticationService.loadToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this._authenticationService.authToken,
      }),
    };
    const updateServerUrl = `${this.baseServerUrl}/${id}`;
    return this._http.put<Server>(updateServerUrl, updateQuery, httpOptions);
  }

  deleteServer(server: Server): Observable<any> {
    this._authenticationService.loadToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this._authenticationService.authToken,
      }),
    };
    const deleteServerUrl = `${this.baseServerUrl}/${server._id}`;
    return this._http.delete<Server>(deleteServerUrl, httpOptions);
  }
}
