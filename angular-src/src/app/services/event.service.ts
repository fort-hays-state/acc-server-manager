import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';
import { Event } from '../models/Event';

@Injectable({
  providedIn: 'root',
})
export class EventService {
  baseEventUrl: string = 'http://localhost:3000/events';

  constructor(
    private _http: HttpClient,
    private _authenticationService: AuthenticationService
  ) {}

  addEvent(event: Event): Observable<any> {
    this._authenticationService.loadToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this._authenticationService.authToken,
      }),
    };
    return this._http.post<Event>(this.baseEventUrl, event, httpOptions);
  }

  getEvents(): Observable<Event[]> {
    this._authenticationService.loadToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this._authenticationService.authToken,
      }),
    };
    return this._http.get<Event[]>(this.baseEventUrl, httpOptions);
  }

  deleteEvent(event: Event): Observable<any> {
    this._authenticationService.loadToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this._authenticationService.authToken,
      }),
    };
    const deleteEventUrl = `${this.baseEventUrl}/${event._id}`;
    return this._http.delete<Event>(deleteEventUrl, httpOptions);
  }
}
