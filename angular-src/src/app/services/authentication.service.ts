import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/User';

import { JwtHelperService } from '@auth0/angular-jwt';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  authToken: any;
  user: User;
  baseAuthUrl: string = 'http://localhost:3000/users';

  constructor(private _http: HttpClient) {}

  registerUser(user: User): Observable<any> {
    const registerUrl = `${this.baseAuthUrl}/register`;
    return this._http.post<User>(registerUrl, user, httpOptions);
  }

  authenticateUser(user: User): Observable<any> {
    const authenticateUrl = `${this.baseAuthUrl}/authenticate`;
    return this._http.post<User>(authenticateUrl, user, httpOptions);
  }

  getProfile(): Observable<any> {
    this.loadToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this.authToken,
      }),
    };
    const profileUrl = `${this.baseAuthUrl}/profile`;
    return this._http.get<User>(profileUrl, httpOptions);
  }

  getUsers(): Observable<any> {
    this.loadToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this.authToken,
      }),
    };
    return this._http.get<User>(this.baseAuthUrl, httpOptions);
  }

  updateUser(id, updateQuery): Observable<any> {
    this.loadToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this.authToken,
      }),
    };
    const updateUserUrl = `${this.baseAuthUrl}/${id}`;
    return this._http.put<User>(updateUserUrl, updateQuery, httpOptions);
  }

  deleteUser(user: User): Observable<any> {
    this.loadToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this.authToken,
      }),
    };
    const deleteUserUrl = `${this.baseAuthUrl}/${user._id}`;
    return this._http.delete<User>(deleteUserUrl, httpOptions);
  }

  storeUserData(token: string, user: User) {
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  loadToken() {
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  loggedIn() {
    const helper = new JwtHelperService();
    this.loadToken();
    return !helper.isTokenExpired(this.authToken);
  }

  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

  isOwner() {
    const user = JSON.parse(localStorage.getItem('user')) as User;
    return user !== null ? user.owner : false;
  }
}
