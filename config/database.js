module.exports = {
    database: 'mongodb://localhost:27017/accsm',
    options: {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
    },
    secret: 'apolloracingclub4*',
}