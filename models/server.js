const mongoose = require("mongoose");

// Assist Rules
const assistRulesSchema = new mongoose.Schema({
  stabilityControlLevelMax: {
    type: Number,
    required: false,
    default: 100,
    min: 0,
    max: 100,
  },
  disableAutoSteer: {
    type: Number,
    required: false,
    default: 0,
    enum: [0, 1],
  },
  disableIdealLine: {
    type: Number,
    required: false,
    default: 0,
    enum: [0, 1],
  },
  disableAutoPitLimiter: {
    type: Number,
    required: false,
    default: 0,
    enum: [0, 1],
  },
  disableAutoGear: {
    type: Number,
    required: false,
    default: 0,
    enum: [0, 1],
  },
  disableAutoClutch: {
    type: Number,
    required: false,
    default: 0,
    enum: [0, 1],
  },
  disableAutoEngineStart: {
    type: Number,
    required: false,
    default: 0,
    enum: [0, 1],
  },
  disableAutoWiper: {
    type: Number,
    required: false,
    default: 0,
    enum: [0, 1],
  },
  disableAutoLights: {
    type: Number,
    required: false,
    default: 0,
    enum: [0, 1],
  },
});

// Configuration
const configurationSchema = new mongoose.Schema({
  tcpPort: {
    type: Number,
    required: true,
    default: 9201,
    min: 1,
  },
  udpPort: {
    type: Number,
    required: true,
    default: 9201,
    min: 1,
  },
  registerToLobby: {
    type: Number,
    required: true,
    default: 0,
    enum: [0, 1],
  },
  maxConnections: {
    type: Number,
    required: true,
    default: 60,
    min: 1,
    max: 85,
  },
  lanDiscovery: {
    type: Number,
    required: true,
    default: 0,
    enum: [0, 1],
  },
  configVersion: {
    type: Number,
    default: 1,
    min: 1,
  },
});

// Settings
const settingsSchema = new mongoose.Schema({
  serverName: {
    type: String,
    required: true,
    trim: true,
  },
  adminPassword: {
    type: String,
    required: true,
    trim: true,
  },
  carGroup: {
    type: String,
    required: true,
    enum: ["FreeForAll", "GT3", "GT4", "Cup", "ST"],
    default: "GT3",
  },
  trackMedalsRequirement: {
    type: Number,
    required: true,
    enum: [0, 1, 2, 3],
    default: 0,
  },
  safetyRatingRequirement: {
    type: Number,
    required: true,
    default: -1,
    min: -1,
    max: 99,
  },
  racecraftRatingRequirement: {
    type: Number,
    required: true,
    default: -1,
    min: -1,
    max: 99,
  },
  password: {
    type: String,
    required: false,
    trim: true,
  },
  spectatorPassword: {
    type: String,
    required: false,
    trim: true,
  },
  maxCarSlots: {
    type: Number,
    required: true,
    default: 30,
    min: 1,
    max: 85,
  },
  dumpLeaderboards: {
    type: Number,
    required: true,
    default: 0,
    enum: [0, 1],
  },
  dumpEntryList: {
    type: Number,
    required: true,
    default: 0,
    enum: [0, 1],
  },
  isRaceLocked: {
    type: Number,
    required: true,
    default: 0,
    enum: [0, 1],
  },
  randomizeTrackWhenEmpty: {
    type: Number,
    required: true,
    default: 0,
    enum: [0, 1],
  },
  centralEntryListPath: {
    type: String,
    required: false,
  },
  allowAutoDQ: {
    type: Number,
    required: true,
    default: 0,
    enum: [0, 1],
  },
  shortFormationLap: {
    type: Number,
    required: false,
    default: 1,
    enum: [0, 1],
  },
  formationLapType: {
    type: Number,
    required: false,
    default: 3,
    enum: [0, 1, 3],
  },
});

// Server
const serverSchema = new mongoose.Schema({
  online: {
    type: Boolean,
    required: true,
    default: false,
  },
  name: {
    type: String,
    required: true,
    trim: true,
    unique: true,
  },
  eventId: {
    type: String,
    required: false,
  },
  hide: {
    type: Boolean,
    required: false,
    default: true,
  },
  configuration: configurationSchema,
  settings: settingsSchema,
  assistRules: assistRulesSchema,
});

const Server = (module.exports = mongoose.model("Server", serverSchema));

module.exports.addServer = function (newServer, callback) {
  newServer.save(callback);
};

module.exports.getServers = function (callback) {
  const query = {};
  Server.find(query, callback);
};

module.exports.updateServer = function (id, updateQuery, callback) {
  Server.findByIdAndUpdate(
    id,
    { ...updateQuery },
    { returnOriginal: false, useFindAndModify: false },
    callback
  );
};

module.exports.deleteServer = function (id, callback) {
  Server.findByIdAndDelete(id, callback);
};
