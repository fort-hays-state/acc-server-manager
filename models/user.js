const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const config = require("../config/database");
const validator = require("validator");

// User
const User = (module.exports = mongoose.model("User", {
  name: {
    type: String,
    required: true,
    trim: true,
  },
  email: {
    type: String,
    required: true,
    lowercase: true,
    unique: true,
    trim: true,
    validate(value) {
      if (!validator.isEmail(value)) {
        throw new Error("Email is invalid");
      }
    },
  },
  username: {
    type: String,
    required: true,
    unique: true,
    trim: true,
  },
  password: {
    type: String,
    required: true,
    minlength: 8,
  },
  authorized: {
    type: Boolean,
    required: true,
    default: false,
  },
  owner: {
    type: Boolean,
    required: true,
    default: false,
  },
}));

module.exports.getUserById = function (id, callback) {
  User.findById(id, callback);
};

module.exports.getUserByUsername = function (username, callback) {
  const query = { username: username };
  User.findOne(query, callback);
};

module.exports.getUsers = function (callback) {
  const query = {};
  User.find(query, callback);
};

module.exports.addUser = function (newUser, callback) {
  bcrypt.genSalt(10, (error, salt) => {
    bcrypt.hash(newUser.password, salt, (error, hash) => {
      if (error) throw error;
      newUser.password = hash;
      newUser.save(callback);
    });
  });
};

module.exports.comparePassword = function (password, hash, callback) {
  bcrypt.compare(password, hash, (error, isMatch) => {
    if (error) throw error;
    callback(null, isMatch);
  });
};

module.exports.updateUser = function (id, updateQuery, callback) {
  User.findByIdAndUpdate(
    id,
    { ...updateQuery },
    { returnOriginal: false, useFindAndModify: false },
    callback
  );
};

module.exports.deleteUser = function (id, callback) {
  User.findByIdAndDelete(id, callback);
};
