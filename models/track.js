const mongoose = require("mongoose");

// Track
const Track = (module.exports = mongoose.model("Track", {
  name: {
    type: String,
    required: true,
    trim: true,
  },
  identifier: {
    type: String,
    required: true,
    unique: true,
    trim: true,
    lowercase: true,
  },
  pitBoxes: {
    type: Number,
    required: true,
    min: 1,
  },
  privateServerSlots: {
    type: Number,
    required: true,
    min: 1,
  },
  season: {
    type: Number,
    required: true,
  },
  hide: {
    type: Boolean,
    require: false,
    default: true,
  },
}));

module.exports.generateIdentifier = function (req, callback) {
  let fqid = req.body.name.toLowerCase().trim().replace(" ", "_");
  if (req.body.season != 2018) {
    fqid += "_" + req.body.season;
  }
  callback(fqid);
};

module.exports.getTrackById = function (id, callback) {
  Track.findById(id, callback);
};

module.exports.getTrackByUniqueName = function (identifier, callback) {
  const query = { identifier: identifier };
  Track.findOne(query, callback);
};

module.exports.addTrack = function (newTrack, callback) {
  newTrack.save(callback);
};

module.exports.getTracks = function (callback) {
  const query = {};
  Track.find(query, callback);
};

module.exports.updateTrack = function (identifier, updatedTrack, callback) {
  const query = { identifier: identifier };
  Track.generateIdentifier({ body: updatedTrack }, updatedIdentifier => {
    updatedTrack.identifier = updatedIdentifier;
    Track.findOneAndUpdate(
      query,
      {
        $set: {
          name: updatedTrack.name,
          identifier: updatedTrack.identifier,
          pitBoxes: updatedTrack.pitBoxes,
          privateServerSlots: updatedTrack.privateServerSlots,
          season: updatedTrack.season,
        },
      },
      { returnOriginal: false, useFindAndModify: false },
      callback
    );
  });
};

module.exports.deleteTrack = function (track, callback) {
  Track.deleteOne(track, callback);
};
