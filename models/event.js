const mongoose = require("mongoose");

// Event Rules
const eventRulesSchema = new mongoose.Schema({
  qualifyStandingType: {
    type: Number,
    required: true,
    enum: [1, 2],
    default: 1,
  },
  pitWindowLengthSec: {
    type: Number,
    required: true,
    default: -1,
    validate(value) {
      if (value === 0 || value < -1) {
        throw new Error(
          "Pit Window Length time must be greater than zero, or -1 to disable feature"
        );
      }
    },
  },
  driverStintTimeSec: {
    type: Number,
    required: true,
    default: -1,
    validate(value) {
      if (value === 0 || value < -1) {
        throw new Error("Driver Stint Time must be greater than zero, or -1 to disable feature");
      }
    },
  },
  mandatoryPitstopCount: {
    type: Number,
    required: true,
    default: 0,
    validate(value) {
      if (value < 0) {
        throw new Error("Mandatory Pitstops must be 0 or greater");
      }
    },
  },
  maxTotalDrivingTime: {
    type: Number,
    required: true,
    default: -1,
    validate(value) {
      if (value === 0 || value < -1) {
        throw new Error("Max Total Driving Time must be 0 or greater, or -1 to disable feature");
      }
    },
  },
  maxDriversCount: {
    type: Number,
    required: true,
    default: 1,
    validate(value) {
      if (value < 1) {
        throw new Error("Max Driver Count must be at least 1");
      }
    },
  },
  isRefuellingAllowedInRace: {
    type: Boolean,
    required: true,
    default: true,
  },
  isRefuellingTimeFixed: {
    type: Boolean,
    required: true,
    default: false,
  },
  isMandatoryPitstopRefuellingRequired: {
    type: Boolean,
    required: true,
    default: false,
  },
  isMandatoryPitstopTyreChangeRequired: {
    type: Boolean,
    required: true,
    default: false,
  },
  isMandatoryPitstopSwapDriverRequired: {
    type: Boolean,
    required: true,
    default: false,
  },
  tyreSetCount: {
    type: Number,
    required: true,
    default: 50,
    min: [1, "At least 1 tyre set is required"],
    max: [50, "No more than 50 tyre sets are allowed"],
  },
});

// Session
const sessionSchema = new mongoose.Schema({
  hourOfDay: {
    type: Number,
    required: true,
    default: 12,
    min: 0,
    max: 23,
  },
  dayOfWeekend: {
    type: Number,
    required: true,
    min: 1,
    max: 3,
  },
  timeMultiplier: {
    type: Number,
    required: true,
    default: 1,
    min: 0,
    max: 24,
  },
  sessionType: {
    type: String,
    maxlength: 1,
    required: true,
    default: "P",
    enum: ["P", "Q", "R"],
  },
  sessionDurationMinutes: {
    type: Number,
    required: true,
    default: 20,
    min: 1,
  },
});

// Event Details
const eventDetailSchema = new mongoose.Schema({
  track: {
    type: String,
    required: true,
  },
  preRaceWaitingTimeSeconds: {
    type: Number,
    required: true,
    min: [30, "Pre Race Waiting Time cannot be less than 30 seconds"],
    default: 60,
  },
  sessionOverTimeSeconds: {
    type: Number,
    required: true,
    default: 120,
    validate(value) {
      if (value < 0) {
        throw new Error("Session Over Time must not be less than 0 seconds");
      }
    },
  },
  ambientTemp: {
    type: Number,
    required: true,
    default: 26,
  },
  cloudLevel: {
    type: Number,
    required: true,
    default: 0.3,
    min: [0.0, "Cloud Level must not be less than 0.0"],
    max: [1.0, "Cloud Level must not be more than 1.0"],
  },
  rain: {
    type: Number,
    required: true,
    default: 0.0,
    min: [0.0, "Rain must not be less than 0.0"],
    max: [1.0, "Rain must not be more than 1.0"],
  },
  weatherRandomness: {
    type: Number,
    required: true,
    default: 3,
    min: [0, "Weather Randomness must not be less than 0"],
    max: [7, "Weather Randomness must not be more than 7"],
  },
  postQualySeconds: {
    type: Number,
    required: true,
    default: 60,
    min: [1, "Post Qualifying Time must be greater than 0 seconds to secure grid spawning"],
  },
  postRaceSeconds: {
    type: Number,
    required: true,
    default: 120,
    min: 0,
  },
  sessions: [sessionSchema],
  configVersion: {
    type: Number,
    required: false,
    default: 1,
  },
});

// Event
const eventSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    unique: true,
  },
  detail: eventDetailSchema,
  rules: eventRulesSchema,
  hide: {
    type: Boolean,
    require: false,
    default: true,
  },
});

const Event = (module.exports = mongoose.model("Event", eventSchema));

module.exports.addEvent = function (newEvent, callback) {
  newEvent.save(callback);
};

module.exports.getEvents = function (callback) {
  const query = {};
  Event.find(query, callback);
};

module.exports.deleteEvent = function (id, callback) {
  Event.findByIdAndDelete(id, callback);
};
