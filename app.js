const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const passport = require("passport");
const mongoose = require("mongoose");
const config = require("./config/database");
const users = require("./routes/users");
const servers = require("./routes/servers");
const events = require("./routes/events");
const tracks = require("./routes/tracks");

const app = express();

// BACKEND SERVER PORT
const port = 3000;

// CONNECT TO DATABASE
// Start an instance of mongod --port=27017  --dbpath="<full path to database folder>"
mongoose
  .connect(config.database, config.options)
  .then(client => {
    console.log(`Connected to database ${config.database}`);
  })
  .catch(error => {
    console.log(`Unable to connect to database:\n${error}`);
  });

// MIDDLEWARE
app.use(cors());
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());
require("./config/passport")(passport);

// STATIC FOLDER
app.use(express.static(path.join(__dirname, "public")));

// ROUTES
app.use("/users", users);
app.use("/servers", servers);
app.use("/events", events);
app.use("/tracks", tracks);

// INDEX ROUTE
app.get("/", (req, res) => {
  res.send("Invalid Endpoint");
});

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "public/index.html"));
});

// START SERVER
app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});
