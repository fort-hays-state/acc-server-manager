const express = require("express");
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const config = require("../config/database");
const User = require("../models/user");

// Authenticate
router.post("/authenticate", (req, res, callback) => {
  const username = req.body.username;
  const password = req.body.password;

  User.getUserByUsername(username, (error, user) => {
    if (error) throw error;
    if (!user) {
      return res.json({ success: false, msg: "User not found" });
    }

    User.comparePassword(password, user.password, (error, isMatch) => {
      if (error) throw error;
      if (isMatch) {
        const token = jwt.sign(user.toJSON(), config.secret, {
          expiresIn: 604800,
        });

        res.json({
          success: true,
          token: "JWT " + token,
          user: {
            id: user._id,
            name: user.name,
            username: user.username,
            email: user.email,
            authorized: user.authorized,
            owner: user.owner,
          },
        });
      } else {
        return res.json({ success: false, msg: "Incorrect login credentials" });
      }
    });
  });
});

// CREATE User
router.post("/register", (req, res, callback) => {
  let newUser = new User({
    name: req.body.name,
    email: req.body.email,
    username: req.body.username,
    password: req.body.password,
  });

  User.addUser(newUser, (error, user) => {
    if (error) {
      res.json({ success: false, msg: "Failed to register user", err: error });
    } else {
      res.json({ success: true, msg: "User registered", user: user });
    }
  });
});

// RETRIEVE Users
router.get("", passport.authenticate("jwt", { session: false }), (req, res, callback) => {
  User.getUsers((error, documents) => {
    if (error) {
      return res.json({ success: false, msg: "Unable to retrieve tracks" });
    }
    res.json(documents);
  });
});

// RETRIEVE User
router.get("/profile", passport.authenticate("jwt", { session: false }), (req, res, callback) => {
  res.send({ user: req.user });
});

// UPDATE User
router.put("/:id", passport.authenticate("jwt", { session: false }), (req, res, callback) => {
  User.updateUser(req.params.id, req.body, (error, document) => {
    if (error) {
      return res.json({ success: false, msg: "Unable to update user" });
    } else if (document) {
      res.json({ success: true, msg: "User successfully updated", user: document });
    }
  });
});

// DELETE User
router.delete("/:id", passport.authenticate("jwt", { session: false }), (req, res, callback) => {
  User.deleteUser(req.params.id, (error, user) => {
    if (error) {
      return res.json({ success: false, msg: "Unable to delete user" });
    } else if (!user) {
      return res.json({ success: false, msg: "User not found" });
    }
    return res.json({ success: true, msg: "User successfully deleted" });
  });
});

module.exports = router;
