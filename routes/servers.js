const express = require("express");
const router = express.Router();
const passport = require("passport");
const Server = require("../models/server");

// CREATE Server
router.post("", passport.authenticate("jwt", { session: false }), (req, res, callback) => {
  let newServer = new Server(req.body);
  Server.addServer(newServer, (error, server) => {
    if (error) {
      console.log(error);
      res.json({ success: false, msg: "Failed to add new server", err: error });
    } else {
      res.json({ success: true, msg: "Server added successfully", server: server });
    }
  });
});

// RETRIEVE Servers
router.get("", passport.authenticate("jwt", { session: false }), (req, res, callback) => {
  Server.getServers((error, documents) => {
    if (error) {
      return res.json({ success: false, msg: "Unable to retrieve servers" });
    }
    res.json(documents);
  });
});

// UPDATE Server
router.put("/:id", passport.authenticate("jwt", { session: false }), (req, res, callback) => {
  Server.updateServer(req.params.id, req.body, (error, document) => {
    if (error) {
      return res.json({ success: false, msg: "Unable to update server" });
    } else if (document) {
      res.json({ success: true, msg: "Server successfully updated", server: document });
    }
  });
});

// DELETE SERVER
router.delete("/:id", passport.authenticate("jwt", { session: false }), (req, res, callback) => {
  Server.deleteServer(req.params.id, (error, server) => {
    if (error) {
      return res.json({ success: false, msg: "Unable to delete server" });
    } else if (!server) {
      return res.json({ success: false, msg: "Server not found" });
    }
    return res.json({ success: true, msg: "Server successfully deleted" });
  });
});

module.exports = router;
