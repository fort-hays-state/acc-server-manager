const express = require("express");
const router = express.Router();
const passport = require("passport");
const Event = require("../models/event");

// CREATE Event
router.post("", passport.authenticate("jwt", { session: false }), (req, res, callback) => {
  let newEvent = new Event(req.body);
  Event.addEvent(newEvent, (error, event) => {
    if (error) {
      res.json({ success: false, msg: "Failed to add new event", err: error });
    } else {
      res.json({ success: true, msg: "Event added successfully", event: event });
    }
  });
});

// RETRIEVE Events
router.get("", passport.authenticate("jwt", { session: false }), (req, res, callback) => {
  Event.getEvents((error, documents) => {
    if (error) {
      return res.json({ success: false, msg: "Unable to retrieve events" });
    }
    res.json(documents);
  });
});

// UPDATE Event

// DELETE Event
router.delete("/:id", passport.authenticate("jwt", { session: false }), (req, res, callback) => {
  Event.deleteEvent(req.params.id, (error, user) => {
    if (error) {
      return res.json({ success: false, msg: "Unable to delete event" });
    } else if (!user) {
      return res.json({ success: false, msg: "Event not found" });
    }
    return res.json({ success: true, msg: "Event successfully deleted" });
  });
});

module.exports = router;
