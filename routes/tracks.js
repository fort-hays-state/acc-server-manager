const express = require("express");
const router = express.Router();
const passport = require("passport");
const Track = require("../models/track");

// CREATE New Track
router.post("/track", passport.authenticate("jwt", { session: false }), (req, res, callback) => {
  Track.generateIdentifier(req, identifer => {
    let newTrack = new Track({
      name: req.body.name,
      identifier: identifer,
      pitBoxes: req.body.pitBoxes,
      privateServerSlots: req.body.privateServerSlots,
      season: req.body.season,
    });

    Track.addTrack(newTrack, (error, track) => {
      if (error) {
        res.json({ success: false, msg: "Failed to add new track", err: error });
      } else {
        res.json({ success: true, msg: "Track added successfully", track: track });
      }
    });
  });
});

// RETRIEVE Tracks
router.get("/track", passport.authenticate("jwt", { session: false }), (req, res, callback) => {
  Track.getTracks((error, documents) => {
    if (error) {
      return res.json({ success: false, msg: "Unable to retrieve tracks" });
    }
    res.json(documents);
  });
});

// UPDATE Track
router.put("/track/:id", passport.authenticate("jwt", { session: false }), (req, res, callback) => {
  Track.updateTrack(req.params.id, req.body, (error, document, _) => {
    if (error) {
      return res.json({ success: false, msg: "Unable to update track" });
    } else if (document) {
      res.json({ success: true, msg: "Track successfully updated", track: document });
    }
  });
});

// DELETE Track
router.delete(
  "/track/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, callback) => {
    Track.getTrackByUniqueName(req.params.id, (error, track) => {
      if (error) throw error;
      if (!track) {
        return res.json({ success: false, msg: "Track not found" });
      }
      Track.deleteTrack(track, error => {
        if (error) throw error;
        return res.json({ success: true, msg: "Track successfully deleted" });
      });
    });
  }
);

module.exports = router;
