To start application:

1. Open 2 terminals in the project root (acc-server-manager) directory

2. In the first terminal start the mongodb service:

    mongod --port=27017 --dbpath="./database"

    * NOTE: Make sure the database directory exists in the project root directory before starting mongod

3. In the second terminal start the application:

    "npm start" or "nodemon"

    * NOTE: If you got the source from Bitbucket you'll need to run "ng build" from the angular-src directory first

4. Open browser and navigate to application:

    http://localhost:3000

5. Create a superuser

    a. From browser register a new user
    b. Using MongoDB Compass, mongocli or equivalent tools update the user:
            
            authorize: true
            owner: true

6. You can now log in and begin using the application

